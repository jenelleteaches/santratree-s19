//
//  GameScene.swift
//  SantraTree-S19
//
//  Created by MacStudent on 2019-06-10.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var oranges:[SKSpriteNode] = []
    
    override func didMove(to view: SKView) {
    }

    func makeOrange(xPosition:CGFloat, yPosition:CGFloat) {
        // lets add some cats
        let orange = SKSpriteNode(imageNamed: "Orange")
        
        // set the position of the orange
        orange.position = CGPoint(x:xPosition, y:yPosition)
        orange.zPosition = 100
        
        // add physics to the orange
        // -------------------------
        
        // 1. create a circular hitbox around the orange
        orange.physicsBody = SKPhysicsBody(circleOfRadius: orange.size.width / 2)
        // By default, a physics body has:
        // -- dynamic = true (show collisions)
        // -- affectedByGravity = true;
        
        
        // add the orange to the scene
        addChild(orange)
        
        // add the oragne to the oranges array
        self.oranges.append(orange)
        
        print("Where is orange? \(xPosition), \(yPosition)")
    }
    
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // detect when you touch the tree
        let locationTouched = touches.first
        
        if (locationTouched == nil) {
            // This is error handling
            // Sometimes the mouse detection doesn't work properly
            // and IOS can't get the position.
            return
        }
        
        let mousePosition = locationTouched!.location(in:self)
        
        print("STARTING (x,y):  \(mousePosition.x), \(mousePosition.y)")
        
        
        // 1. Detect what sprite the person touched
        let spriteTouched = self.atPoint(mousePosition)
        //print("YOU TOUCHED: \(spriteTouched)")

        // 2. Check if he touched the tree
        if (spriteTouched.name == "tree") {
            print("TREE TOUCHED!")
   
            // Make an orange!
            makeOrange(xPosition: mousePosition.x, yPosition: mousePosition.y)
        }
        
        
        
    } // end touchesBegan
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {

        // where does person release the mouse?
        let locationTouched = touches.first
        
        if (locationTouched == nil) {
            // This is error handling
            // Sometimes the mouse detection doesn't work properly
            // and IOS can't get the position.
            return
        }
        
        let mousePosition = locationTouched!.location(in:self)
        
        print("ENDING (x,y):  \(mousePosition.x), \(mousePosition.y)")
        print("------")
    }
    
    
    
    
    
    
    

}
